package com.iomis.qjimbo.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.iomis.qjimbo.R;

public class AccountCreated extends AppCompatActivity {

    TextView HelloText;
    String username;
Button loading;
Animation bounce;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_created);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        loading = (Button) findViewById(R.id.loading);

        Intent intent = getIntent();
        username = intent.getStringExtra("UserFirstname");
        HelloText = (TextView) findViewById(R.id.HelloText);

        HelloText.setText("Hi, "+username+"."
        +"Account created successfully.\n" + "\n"+"Welcome to our ciname library!"
        );
        bounce = AnimationUtils.loadAnimation(this, R.anim.bounce);
        loading.startAnimation(bounce);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
               loading.clearAnimation();
                loading.setAlpha(1);
                Intent intent = new Intent(AccountCreated.this, SignIn.class);
                intent.putExtra("Username",username);
                startActivity(intent);
                finish();

            }
        }, 3000);

    }
}
