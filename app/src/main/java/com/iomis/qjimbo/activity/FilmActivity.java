package com.iomis.qjimbo.activity;



import android.content.ClipData;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.icu.text.IDNA;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.*;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.iomis.qjimbo.R;
import com.iomis.qjimbo.adapters.*;
import com.iomis.qjimbo.utils.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;

import  com.bumptech.glide.Glide;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;



public class FilmActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = FilmActivity.class.getSimpleName();
    private BottomNavigationView mBottomNav;


    private int GALLERY = 1;
    private List<Movie> movies;
    private RecyclerView recyclerView;
    byte [] avatarByteArr;
    private MoviesAdapter adapter;
    TextView username, nav_email;
    String nickname, emailfromdb;
    ImageView avatar;
    int resourceCardNewFilms,resourceCardPosts,resourceCardPosters,resourceCardGenre;

    Bundle bundle;


    RecyclerView recyclerViewPosters;
    RecyclerView recyclerViewrecomend;
    RecyclerView recyclerViewPosts;
    RecyclerView recyclerViewGenre;


    //User info
    ContentValues cv;
    SQLiteDatabase db;
    DBHelper dbHelper;
    DatabaseFinder dbfinder;
    Cursor avatarCursor;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
        //set your username on navigation drawer
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);

        username = (TextView) headerView.findViewById(R.id.nav_username);
        avatar = (ImageView) headerView.findViewById(R.id.avatar);
        nav_email = (TextView) headerView.findViewById(R.id.nav_email);
        recyclerView = findViewById(R.id.recyclerview);
        recyclerViewrecomend = findViewById(R.id.recyclerviewRecomend);
        recyclerViewPosters = findViewById(R.id.recyclerviewPosters);
        recyclerViewPosts = findViewById(R.id.recyclerviewInteresting);
        recyclerViewGenre = findViewById(R.id.recyclerviewGenre);

        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(recyclerViewPosters);
        SnapHelper snapHelper1 = new LinearSnapHelper();
        snapHelper1.attachToRecyclerView(recyclerViewPosts);





        avatar.setOnClickListener(this);
        //get username from intent from db
        Intent intent = getIntent();
        nickname = intent.getStringExtra("Username");
        emailfromdb = intent.getStringExtra("Email");
        avatarByteArr = intent.getByteArrayExtra("Avatar");
        username.setText(nickname);
        nav_email.setText(emailfromdb);
        resourceCardNewFilms = R.layout.card;
        resourceCardPosters = R.layout.long_card;
        resourceCardPosts = R.layout.posts_card;
        resourceCardGenre = R.layout.genre_card;


        cv = new ContentValues();
        dbfinder = new DatabaseFinder();
        updateAvatar();

recyclerView.setNestedScrollingEnabled(false);
recyclerViewrecomend.setNestedScrollingEnabled(false);
recyclerViewPosters.setNestedScrollingEnabled(false);
recyclerViewPosts.setNestedScrollingEnabled(false);
recyclerViewGenre.setNestedScrollingEnabled(false);
        movies = new ArrayList<>();
       // getMoviesFromDB(0);

        bundle = ActivityOptionsCompat.makeCustomAnimation(getApplicationContext(),
                android.R.anim.fade_in, android.R.anim.fade_out).toBundle();


initBottomNavigationClick();

        InfoForCards InfoNewFilms = new InfoForCards();
        InfoNewFilms.NewFilmsInitImageBitmaps();
        initImageBitmapsNewFilms(recyclerView,InfoNewFilms,resourceCardNewFilms);


        InfoForCards InfoPosters = new InfoForCards();
        InfoPosters.InitPosters();
        initImagePosters(recyclerViewPosters,InfoPosters,resourceCardPosters);

        InfoForCards InfoRecomended = new InfoForCards();
        InfoRecomended.RecomendedFilmsInitImageBitmaps();
        initImageBitmapsNewFilms(recyclerViewrecomend,InfoRecomended,resourceCardNewFilms);

        InfoForCards InfoInterestingPosts = new InfoForCards();
        InfoInterestingPosts.InitInterestingPosts();
        initPosts(recyclerViewPosts,InfoInterestingPosts,resourceCardPosts);

        InfoForCards InfoGenre = new InfoForCards();
        InfoGenre.InitGenres();
initGenreRV(recyclerViewGenre,InfoGenre,resourceCardGenre);

    }




    public void initImageBitmapsNewFilms(RecyclerView recyclerView, InfoForCards InfoNewFilms,int resourceCardNewFilms){


        initRecyclerView(resourceCardNewFilms,recyclerView,InfoNewFilms.getmNames(),InfoNewFilms.getmImageUrls(), InfoNewFilms.getmGenre(),InfoNewFilms.getDescription(),InfoNewFilms.getYear(),InfoNewFilms.getImdb(),InfoNewFilms.getTrailerUrls());
    }

    public void initImagePosters(RecyclerView recyclerView, InfoForCards InfoNewFilms,int resourceCardNewFilms){

        initRecyclerViewPosters(resourceCardNewFilms,recyclerView,InfoNewFilms.getmImageUrls());
    }

    public void initPosts(RecyclerView recyclerView, InfoForCards InfoNewFilms,int resourceCardNewFilms){

        initRecyclerViewPosts(resourceCardNewFilms,recyclerView,InfoNewFilms.getmImageUrls(),InfoNewFilms.getmNames());
    }

    public void initGenreRV(RecyclerView recyclerView, InfoForCards InfoNewFilms,int resourceCardNewFilms){

        initRecyclerViewGenre(resourceCardNewFilms,recyclerView,InfoNewFilms.getmImageUrls(),InfoNewFilms.getmNames());
    }


    private void initBottomNavigationClick(){

        mBottomNav = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        mBottomNav.setSelectedItemId(R.id.action_catalog);
        mBottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                selectFragment(item);
                return true;
            }
        });


    }


    private void selectFragment(MenuItem item) {
        Fragment frag = null;
        // init corresponding fragment
        switch (item.getItemId()) {
            case R.id.action_catalog:

                break;
            case R.id.action_watch_list:
               Intent intent = new Intent(FilmActivity.this,WatchListActivity.class);
                startActivity(intent,bundle);
               finish();
                break;
            case R.id.action_favourites:
                Intent intent2 = new Intent(FilmActivity.this,FavouritesActivity.class);
                startActivity(intent2,bundle);
                finish();
                break;
        }



    }



    private void initRecyclerView(int resource,RecyclerView recyclerView, ArrayList<String> mNames, ArrayList<String> mImageUrls,ArrayList<String> mGenre,ArrayList<String> description,ArrayList<String> year,ArrayList<String> imdb,ArrayList<String> TrailerUrls){
        Log.d(TAG, "initRecyclerView: init recyclerview.");
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(this, mNames, mImageUrls,mGenre,description,year,imdb,TrailerUrls,resource);
       recyclerView.setAdapter(adapter);
      recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
    }



    private void initRecyclerViewPosters(int resource, final RecyclerView recyclerView, final ArrayList<String> mImageUrls){
        Log.d(TAG, "initRecyclerView: init recyclerview.");
        final RecyclerViewLongAdapter adapter = new RecyclerViewLongAdapter(this, mImageUrls,resource);
        recyclerView.setAdapter(adapter);
       final SlowLinearLayoutManager linearLayoutManager = new SlowLinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setLayoutManager(linearLayoutManager);

//Endless mode of RecyclerView  ( example : 1,2,3,4....end...1,2,3,4..)
EndlessRecyclerView.MakeEndlessRecyclerView(recyclerView,linearLayoutManager,mImageUrls.size());

recyclerView.scrollToPosition(1);
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new AutoScrollTask(recyclerView,1,mImageUrls.size()), 3000, 6000);

    }

    private void initRecyclerViewPosts(int resource, final RecyclerView recyclerView, ArrayList<String> mImageUrls,ArrayList<String> mNames){

        RecyclerViewPostsAdapter adapter = new RecyclerViewPostsAdapter(this, mImageUrls,mNames,resource);
        recyclerView.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setLayoutManager(linearLayoutManager);

//Endless mode of RecyclerView  ( example : 1,2,3,4....end...1,2,3,4..)
        EndlessRecyclerView.MakeEndlessRecyclerView(recyclerView,linearLayoutManager,mImageUrls.size());

    }


    private void initRecyclerViewGenre(int resource, final RecyclerView recyclerView, ArrayList<String> mImageUrls,ArrayList<String> mNames){

        RecyclerViewGenreAdapter adapter = new RecyclerViewGenreAdapter(this, mImageUrls,mNames,resource);
        recyclerView.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setLayoutManager(linearLayoutManager);

    }




public void updateAvatar(){
        if(avatarByteArr!=null){


        Bitmap mBitmap = BitmapFactory.decodeByteArray(avatarByteArr, 0, avatarByteArr.length);
        avatar.setImageBitmap(mBitmap);
    }}




    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);


                    avatar.setImageBitmap(bitmap);

                    byte[] avatarByteArr = getBitmapAsByteArray(bitmap);
                 cv.put("avatar",avatarByteArr);
                    dbHelper = new DBHelper(this);
                    db = dbHelper.getWritableDatabase();
                    db.update("UserRegisterInfo", cv, " email = '"+emailfromdb+ "'", null);
                    db.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(FilmActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        }
    }




    public static byte[] getBitmapAsByteArray(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 0, outputStream);
        return outputStream.toByteArray();
    }




    @Override
    public void onClick (View view){
        switch (view.getId()) {

            case R.id.avatar:
                choosePhotoFromGallary();

                break;
        }

    }





   /*    private void getMoviesFromDB(int id) {

        AsyncTask<Integer, Void, Void> asyncTask = new AsyncTask<Integer, Void, Void>() {
            @Override
            protected Void doInBackground(Integer... movieIds) {

                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("http://192.168.43.161/movies.php?id=" + movieIds[0])
                        .build();
                try {
                    Response response = client.newCall(request).execute();

                    JSONArray array = new JSONArray(response.body().string());

                    for (int i = 0; i < array.length(); i++) {

                        JSONObject object = array.getJSONObject(i);

                        Movie movie = new Movie(object.getInt("id"), object.getString("movie_name"),
                                object.getString("movie_image"), object.getString("movie_genre"));

                        FilmActivity.this.movies.add(movie);

                    }


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                adapter.notifyDataSetChanged();
            }
        };

        asyncTask.execute(id);
    }*/








}
