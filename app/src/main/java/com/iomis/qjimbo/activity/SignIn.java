package com.iomis.qjimbo.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.*;
import com.iomis.qjimbo.R;
import com.iomis.qjimbo.activity.DatabaseFinder;
import com.iomis.qjimbo.utils.DBHelper;
import com.iomis.qjimbo.utils.GetTextFromField;
import com.iomis.qjimbo.utils.MessageAboutEditTextEror;

public class SignIn extends AppCompatActivity implements View.OnClickListener {


    private static final String TABLE_NAME = "UserRegisterInfo";
    private static final String COLUMN_USERNAME = "username";
    private static final String COLUMN_EMAIL = "email";
    private static final String COLUMN_PASSWORD = "password";

Cursor u,c;
    Animation bounce;
    Button signIn;
    EditText EmailInput,PasswordInput;
    DBHelper dbHelper;
    SQLiteDatabase db;

    AlertDialog.Builder EditTextFieldNullExeption;
    String username,emailtoTransfer;
    String email;
    byte [] avatarByteArr;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        signIn = (Button) findViewById(R.id.signInButton);
        EmailInput=(EditText) findViewById(R.id.email_input);
        PasswordInput=(EditText) findViewById(R.id.password_input);
        signIn.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        //Animation to our Create Account button
        bounce = AnimationUtils.loadAnimation(this, R.anim.bounce);

        switch (view.getId()) {

            case R.id.signInButton:

                email = EmailInput.getText().toString().trim();
                final String[] RegisterInfo = new String[2];
                GetTextFromField Register = new GetTextFromField();
                Register.getTextFromEditText(EmailInput,PasswordInput);
                Register.getAllStringSignIn(RegisterInfo);
                dbHelper = new DBHelper(this);
                db = dbHelper.getReadableDatabase();
                DatabaseFinder dbfinder = new DatabaseFinder();
                c = dbfinder.findInDatabaseEmail(db,RegisterInfo,c);

                 u = dbfinder.findInDatabaseEmail(db,RegisterInfo,u);






               final Intent intent = new Intent(SignIn.this,SignInSuccessfully.class);
                if (MainActivity.isNotEmpty(EmailInput) && MainActivity.isNotEmpty(PasswordInput)) {
                    if (email.matches(emailPattern)) {
                        if (PasswordInput.getText().toString().trim().length() > 5) {
                            if(c.getCount()==1) {

                                if(u.moveToFirst()) {
                                    username = u.getString(2);
                                    emailtoTransfer = u.getString(3);
                                    avatarByteArr = u.getBlob(5);
                                    u.close();
                                    c.close();
                                    db.close();
                                }
                                signIn.setClickable(false);
                                signIn.startAnimation(bounce);
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    public void run() {
                                        intent.putExtra("Username",username);
                                        intent.putExtra("Email",emailtoTransfer);
                                        intent.putExtra("Avatar",avatarByteArr);
                                        Bundle bundle = ActivityOptionsCompat.makeCustomAnimation(getApplicationContext(),
                                                android.R.anim.fade_in, android.R.anim.fade_out).toBundle();
                                        startActivity(intent,bundle);
                                        signIn.clearAnimation();
                                        signIn.setAlpha(1);
                                        signIn.setClickable(true);

                                    }
                                }, 3000);
                            }
                            else{
                                EditTextFieldNullExeption = new AlertDialog.Builder(SignIn.this, R.style.AlertDialogCustom);
                                MessageAboutEditTextEror emailErorMessage = new MessageAboutEditTextEror();
                                emailErorMessage.ErorMessageInputInfo(EditTextFieldNullExeption, "Something went wrong\nCheck your password or email", "Error login info");
                            }
                        }
                            else {
                                EditTextFieldNullExeption = new AlertDialog.Builder(SignIn.this, R.style.AlertDialogCustom);
                                MessageAboutEditTextEror emailErorMessage = new MessageAboutEditTextEror();
                                emailErorMessage.ErorMessageInputInfo(EditTextFieldNullExeption, "Something went wrong\nPassword need to be at least 6 symbols", "Error password!");
                            }
                        }

                        //Email input wrong
                        else {
                            //Make Dialog Message if something went wrong( Email input failed)
                            EditTextFieldNullExeption = new AlertDialog.Builder(SignIn.this, R.style.AlertDialogCustom);
                            MessageAboutEditTextEror emailErorMessage = new MessageAboutEditTextEror();
                            emailErorMessage.ErorMessageInputInfo(EditTextFieldNullExeption, "Something went wrong\nEmail input need to be : \ntext@nameoftheemail.com or .net or else", "Error email input!");
                        }
                    } else {
                        //Make Dialog Message if something went wrong( One of the field is clear)
                        EditTextFieldNullExeption = new AlertDialog.Builder(SignIn.this, R.style.AlertDialogCustom);
                        MessageAboutEditTextEror OneFieldIsClear = new MessageAboutEditTextEror();
                        OneFieldIsClear.ErorMessageInputInfo(EditTextFieldNullExeption, "Something went wrong\nOne of the fields is not ready", "Error!");
                    }


                            break;

        }
    }
}
