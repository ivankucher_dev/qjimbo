package com.iomis.qjimbo.activity;

import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.*;
import com.iomis.qjimbo.R;
import com.iomis.qjimbo.utils.DBHelper;
import com.iomis.qjimbo.utils.GetTextFromField;
import com.iomis.qjimbo.utils.MessageAboutEditTextEror;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    //Declarate data
    private Button createacc;
    private Button AllreadyMember;



    private EditText FullName, Lastname, Email, Password;
    private static final String TABLE_NAME = "UserRegisterInfo";
    private static final String COLUMN_EMAIL = "email";
    private static final String COLUMN_USERNAME = "username";
    private static final String COLUMN_PASSWORD = "password";

    private ImageView LogoImage, TextLogoImage;
    final String LOG_TAG = "myLogs";

    Animation bounce;
    Intent intent;
    ActivityOptions optione;
    GetTextFromField Register;

    //Message about Errors
    AlertDialog.Builder EditTextFieldNullExeption;

    //Check to valid email adress (Declarate some data)
    String email;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    //Connection to database
    DBHelper dbHelper;

    //User info
    ContentValues cv;
    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        createacc = (Button) findViewById(R.id.createacc);
        createacc.setOnClickListener(this);
        AllreadyMember = (Button) findViewById(R.id.alreadymemberbutton);
        AllreadyMember.setOnClickListener(this);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        FullName = (EditText) findViewById(R.id.firstname);
        FullName.setOnClickListener(this);
        Lastname = (EditText) findViewById(R.id.lastname);
        Lastname.setOnClickListener(this);
        Email = (EditText) findViewById(R.id.email_input);
        Email.setOnClickListener(this);
        Password = (EditText) findViewById(R.id.password_input);
        Password.setOnClickListener(this);
        LogoImage = (ImageView) findViewById(R.id.imageView);
        TextLogoImage = (ImageView) findViewById(R.id.imageView2);


//delete after
      LogoImage.setOnClickListener(this);

        cv = new ContentValues();
        FullName.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == 66) {
                    FullName.requestFocus();
                }
                return false;
            }
        });

    }




    @Override
    public void onClick(View view) {

        //Animation to our Create Account button
        bounce = AnimationUtils.loadAnimation(this, R.anim.bounce);

        switch (view.getId()) {

            case R.id.createacc:
                email = Email.getText().toString().trim();
                EditTextFieldNullExeption = new AlertDialog.Builder(MainActivity.this);
                intent = new Intent(this, AccountCreated.class);

                final String[] RegisterInfo = new String[5];
                GetTextFromField Register = new GetTextFromField();
                Register.getTextFromEditText(FullName, Lastname, Email, Password);
                Register.getAllStrings(RegisterInfo);
                dbHelper = new DBHelper(this);
                db = dbHelper.getReadableDatabase();


                //Check in database email is already in use
                String selectQuery = "SELECT  * FROM " + TABLE_NAME + " WHERE "
                        + COLUMN_EMAIL + " =\'" + RegisterInfo[2] +"\'";
                Cursor c = db.rawQuery(selectQuery,null);


                //check in database username is already in use
                String selectQueryUser = "SELECT  * FROM " + TABLE_NAME + " WHERE "
                        + COLUMN_USERNAME + " =\'" + RegisterInfo[1] +"\'";
                Cursor u = db.rawQuery(selectQueryUser,null);



                //this is block of code when we checking for errors and registering user
                if (isNotEmpty(FullName) && isNotEmpty(Lastname) && isNotEmpty(Email) && isNotEmpty(Password)) {

                    if (email.matches(emailPattern)) {

                        if (Password.getText().toString().trim().length() > 5) {
                            //if email is new (not yet used)
                            if (c.getCount() == 0) {
                                //if username is new (not yet used)
                                if(u.getCount()== 0){

                                createacc.setText("Loading...");
                                createacc.setClickable(false);
                                createacc.startAnimation(bounce);
                                c.close();
                                u.close();
                                cv.put("fullname", RegisterInfo[0]);
                                cv.put("username", RegisterInfo[1]);
                                cv.put("email", RegisterInfo[2]);
                                cv.put("password", RegisterInfo[3]);
                                cv.put("avatar", RegisterInfo[4]);
                                dbHelper = new DBHelper(this);
                                db = dbHelper.getWritableDatabase();

                                //Insert in Database
                                long rowID = db.insert("UserRegisterInfo", null, cv);
                                Log.d(LOG_TAG, "row inserted, ID = " + rowID);
                                db.close();
                                //Animation to transite our object to the next activity
                                Pair[] paire = new Pair[1];
                                paire[0] = new Pair<View, String>(LogoImage, "imageTransition");
                                optione = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this, paire);

                                //New thread (Waiting add information to our DB)
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    public void run() {
                                        intent.putExtra("UserFirstname", RegisterInfo[0]);

                                        startActivity(intent, optione.toBundle());
                                        createacc.clearAnimation();
                                        createacc.setAlpha(1);
                                        createacc.setText("Create Account");
                                        createacc.setClickable(true);
                                    }
                                }, 3000);
                            }
                            //Output info : this username is aleady in use
                                else {
                                    EditTextFieldNullExeption = new AlertDialog.Builder(MainActivity.this, R.style.AlertDialogCustom);
                                    MessageAboutEditTextEror emailErorMessage = new MessageAboutEditTextEror();
                                    emailErorMessage.ErorMessageInputInfo(EditTextFieldNullExeption, "Something went wrong\nUsername already in use, try a different one", "Username Error!");
                                }
                            }
                            //Output info : Member with this email is already registered
                            else {
                                EditTextFieldNullExeption = new AlertDialog.Builder(MainActivity.this, R.style.AlertDialogCustom);
                                MessageAboutEditTextEror emailErorMessage = new MessageAboutEditTextEror();
                                Intent intent3 = new Intent(this, SignIn.class);
                                emailErorMessage.ErorMessageEmail(EditTextFieldNullExeption, "Something went wrong\nThis user is already a member, try to sign in", "Error register!",intent3,this);
                            }
                        }

                        //Output info : Password less then 6 symbols
                        else {
                            EditTextFieldNullExeption = new AlertDialog.Builder(MainActivity.this, R.style.AlertDialogCustom);
                            MessageAboutEditTextEror emailErorMessage = new MessageAboutEditTextEror();
                            emailErorMessage.ErorMessageInputInfo(EditTextFieldNullExeption, "Something went wrong\nPassword need to be at least 6 symbols", "Error password!");
                        }
                    }

                    // Output info : Email input wrong
                    else {
                        //Make Dialog Message if something went wrong( Email input failed)
                        EditTextFieldNullExeption = new AlertDialog.Builder(MainActivity.this, R.style.AlertDialogCustom);
                        MessageAboutEditTextEror emailErorMessage = new MessageAboutEditTextEror();
                        emailErorMessage.ErorMessageInputInfo(EditTextFieldNullExeption, "Something went wrong\nEmail input need to be : \ntext@nameoftheemail.com or .net or else", "Error email input!");
                    }
                } else {
                    //Make Dialog Message if something went wrong( One of the field is clear)
                    EditTextFieldNullExeption = new AlertDialog.Builder(MainActivity.this, R.style.AlertDialogCustom);
                    MessageAboutEditTextEror OneFieldIsClear = new MessageAboutEditTextEror();
                    OneFieldIsClear.ErorMessageInputInfo(EditTextFieldNullExeption, "Something went wrong\nOne of the fields is not ready", "Error!");
                }


                break;

            case R.id.alreadymemberbutton:
                Intent intent2 = new Intent(this, SignIn.class);

                Pair[] pairs = new Pair[4];
                pairs[0] = new Pair<View, String>(LogoImage, "imageTransition");
                pairs[1] = new Pair<View, String>(TextLogoImage, "textTransition");
                pairs[2] = new Pair<View, String>(LogoImage, "EmailTransition");
                pairs[3] = new Pair<View, String>(LogoImage, "PasswordTransition");

                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this, pairs);
                startActivity(intent2, options.toBundle());

                break;

    //!!!!           //DELETE AFTER DEVELOPMENT ( FOR DEVELOPER USER TO FAST TAKE FILMACTVIITy)
            case R.id.imageView :
                Intent intent3 = new Intent(this,FilmActivity.class);
                startActivity(intent3);
                break;

            default:
                break;
        }

    }

    public static boolean isNotEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0)
            return true;

        return false;
    }

}
