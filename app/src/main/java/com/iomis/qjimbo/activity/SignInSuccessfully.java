package com.iomis.qjimbo.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import com.iomis.qjimbo.R;

public class SignInSuccessfully extends AppCompatActivity {

    TextView HelloText;
    String username,email;
    byte [] avatarByteArr;
    Animation bounce;
    Button loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in_successfully);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        loading = (Button) findViewById(R.id.loading);
        Intent intent = getIntent();
         username = intent.getStringExtra("Username");
       email = intent.getStringExtra("Email");
       avatarByteArr = intent.getByteArrayExtra("Avatar");
        HelloText = (TextView) findViewById(R.id.HelloText);
        HelloText.setText("Hi, "+username+"! "
                +"Sign in successfully.\n"
        );
        //Animation to our Create Account button
        bounce = AnimationUtils.loadAnimation(this, R.anim.bounce);
        loading.startAnimation(bounce);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                loading.clearAnimation();
                loading.setAlpha(1);
                Intent intent = new Intent(SignInSuccessfully.this, FilmActivity.class);
                intent.putExtra("Username",username);
                intent.putExtra("Email",email);
                intent.putExtra("Avatar",avatarByteArr);
                startActivity(intent);
finish();
            }
        }, 3000);

    }
}
