package com.iomis.qjimbo.UI;


import android.content.pm.ActivityInfo;
import android.view.WindowManager;
import com.iomis.qjimbo.R;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.iomis.qjimbo.utils.PlayerConfig;


public class Trailer extends YouTubeBaseActivity {

    YouTubePlayerView youTubePlayerView;
    Button button;
    YouTubePlayer.OnInitializedListener onInitializedListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trailer);
setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);

        youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_player);



        onInitializedListener = new YouTubePlayer.OnInitializedListener(){

            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {

                youTubePlayer.loadVideo(getIntent().getStringExtra("trailer"));

                youTubePlayer.play();
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

            }
        };

                youTubePlayerView.initialize(PlayerConfig.API_KEY,onInitializedListener);



    }



}