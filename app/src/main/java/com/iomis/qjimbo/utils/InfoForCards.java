package com.iomis.qjimbo.utils;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import com.iomis.qjimbo.R;
import com.iomis.qjimbo.adapters.RecyclerViewAdapter;


import java.util.ArrayList;

public class InfoForCards {

    public ArrayList<String> mNames = new ArrayList<>();
    public ArrayList<String> mGenre = new ArrayList<>();
    public ArrayList<String> mImageUrls = new ArrayList<>();
    public ArrayList<String> Description = new ArrayList<>();
    public ArrayList<String> year = new ArrayList<>();
    public ArrayList<String> imdb = new ArrayList<>();
    public ArrayList<String> TrailerUrls = new ArrayList<>();

    public ArrayList<String> getTrailerUrls() {
        return TrailerUrls;
    }

    public ArrayList<String> getYear() {
        return year;
    }

    public ArrayList<String> getImdb() {
        return imdb;
    }

    public ArrayList<String> getmNames() {
        return mNames;
    }

    public ArrayList<String> getmGenre() {
        return mGenre;
    }

    public ArrayList<String> getmImageUrls() {
        return mImageUrls;
    }

    public ArrayList<String> getDescription() {
        return Description;
    }

    public void NewFilmsInitImageBitmaps(){




        mImageUrls.add("https://my-hit.org/storage/2014774_210x300x50x2.jpg");
        mNames.add("Aquaman");
        mGenre.add("Fantasy");
        Description.add("Aquaman is a 2018 DC Comics,DC Entertainment,Panoramic Pictures. If you are watching the calendar of premieres of superhero films, you will know that one of the two films that are ahead of us this Christmas is Aquaman. The solo movie of the King of Atlantis, DC Comics' underwater superhero and founding member of the JLA, is one of Warner Bros.'s big bets to successfully continue his campaign at the box office against Marvel Studios after the lukewarm reception of the League of Justice. In many ways, the premiere of the next three films based on DC Comics characters will determine the future of these characters in the cinema. That is, the aforementioned Aquaman, Shazam! and Wonder Woman 1984. (the movie of the Joker we do not count it, since it has another look different from that of the superheroic genre per se). In many ways, Aquaman can determine the advertising investment, but also the treatment of future superheroes (and supervillains) of DC Comics in the cinema. While the animated films of DC are still recognized as the best of the genre today, the real action productions do not quite convince unanimously neither the public nor the critics, divided between those who hate them and those who defend them. with cape and sword, leaving to those who maintain a critical and weighted spirit in minority.\n" +
                "\nDirector: James Wan\n" +
                "Writers: David Leslie Johnson-McGoldrick (screenplay by), Will Beall (screenplay by)\n" +
                "Stars: Jason Momoa, Amber Heard, Willem Dafoe");
        year.add("2018");
        imdb.add("7,9");
        TrailerUrls.add("OI8hmYbF4Hc");


        mImageUrls.add("https://my-hit.org/storage/2019595_210x300x50x2.jpg");
        mNames.add("Replicas");
        mGenre.add("Fantasy,Triller");
        Description.add("Replicas is a 2018 American science fiction thriller film directed by Jeffrey Nachmanoff and written by Chad St. John from a story by Stephen Hamel. The film tells the story of a neuroscientist who violates the law and biomedical ethics to bring his family members back to life after they die in a car accident." +
                "\n\nDirector: Jeffrey Nachmanoff\n" +
                "Writers: Chad St. John (screenplay by), Stephen Hamel (story by)\n" +
                "Stars: Alice Eve, Keanu Reeves, Emily Alyn Lind");
        year.add("2018");
        imdb.add("6,8");
        TrailerUrls.add("sWyrndWCdWE");


        mImageUrls.add("https://my-hit.org/storage/2024464_210x300x50x2.jpg");
        mNames.add("Bird Box");
        mGenre.add("Drama, Horror,Fantasy");
        Description.add("In the wake of an unknown global terror, a mother must find the strength to flee with her children down a treacherous river in search of safety. Due to unseen deadly forces, the perilous journey must be made blindly. Directed by Emmy winner Susanne Bier, Bird Box is a thriller starring Academy Award winner Sandra Bullock, John Malkovich, Sarah Paulson, and Trevante Rhodes. Written by Netflix" +
                "\n\nDirector: Susanne Bier\n" +
                "Writers: Eric Heisserer (screenplay), Josh Malerman (novel)\n" +
                "Stars: Sandra Bullock, Trevante Rhodes, John Malkovich");
        year.add("2018");
        imdb.add("7,1");
        TrailerUrls.add("1mrjPl7Ijzs");


        mImageUrls.add("https://my-hit.org/storage/2001759_210x300x50x2.jpg");
        mNames.add("Mortal Engines");
        mGenre.add("Fantasy");
        Description.add("A mysterious young woman, Hester Shaw, emerges as the only one who can stop a giant, predator city on wheels devouring everything in its path. Feral, and fiercely driven by the memory of her mother, Hester joins forces with Tom Natsworthy, an outcast from London, along with Anna Fang, a dangerous outlaw with a bounty on her head." +
                "\n\nDirector: Christian Rivers\n" +
                "Writers: Fran Walsh (screenplay by), Philippa Boyens (screenplay by) \n" +
                "Stars: Hera Hilmar, Robert Sheehan, Hugo Weaving ");
        year.add("2018");
        imdb.add("7,2");
        TrailerUrls.add("ucg9tajl3AE");


        mImageUrls.add("https://my-hit.org/storage/2013029_210x300x50x2.jpg");
        mNames.add("Upgrade");
        mGenre.add("Action,Fantasy");
        Description.add("Director/writer Leigh Whannell presents a vision of a Utopian future where technology is supremely ubiquitous. In that future, technophobe Grey Trace (Logan Marshall-Green) embraces an experimental computer chip implant named STEM after a mugging incident leaves him paralyzed and his wife dead. STEM proves to possess its own agency, making for an unpredictable ally in Trace's vendetta." +
                "\n\nDirector: Leigh Whannell\n" +
                "Writer: Leigh Whannell\n" +
                "Stars: Logan Marshall-Green, Melanie Vallejo, Steve Danielsen");
        year.add("2018");
        imdb.add("6,9");
        TrailerUrls.add("Oo4oqHeOnAw");


        mImageUrls.add("https://my-hit.org/storage/2014314_210x300x50x2.jpg");
        mNames.add("Venom");
        mGenre.add("Horror,Fantasy,Action");
        Description.add("Investigative journalist Eddie Brock attempts a comeback following a scandal, but accidentally becomes the host of an alien symbiote that gives him a violent super alter-ego: Venom. Soon, he must rely on his newfound powers to protect the world from a shadowy organisation looking for a symbiote of their own.\n" +
                "\n\nRating:\tPG-13 (for intense sequences of sci-fi violence and action, and for language)\n" +
                "Directed By:\tRuben Fleischer\n" +
                "Written By:\tScott Rosenberg, Jeff Pinkner, Kelly Marcel, Will Beall\n" +
                "In Theaters:\tOct 5, 2018  Wide\n" +
                "On Disc/Streaming:\tDec 18, 2018\n" +
                "Runtime:\t112 minutes\n" +
                "Studio:\tColumbia Pictures");
        year.add("2018");
        imdb.add("7,7");
        TrailerUrls.add("B9_DhonlVQQ");


        mImageUrls.add("https://my-hit.org/storage/2001761_210x300x50x2.jpg");
        mNames.add("Bumblebee");
        mGenre.add("Fantasy,Action");
        Description.add("On the run in the year 1987, Bumblebee finds refuge in a junkyard in a small Californian beach town. Charlie (Hailee Steinfeld), on the cusp of turning 18 and trying to find her place in the world, discovers Bumblebee, battle-scarred and broken. When Charlie revives him, she quickly learns this is no ordinary, yellow VW bug. Written by Paramount Pictures" +
                "\n\nDirector: Travis Knight\n" +
                "Writers: Christina Hodson, Christina Hodson (story by)\n" +
                "Stars: Hailee Steinfeld, Jorge Lendeborg Jr., John Cena");
        year.add("2018");
        imdb.add("7,0");
        TrailerUrls.add("lcwmDAYt22k");


        mImageUrls.add("https://my-hit.org/storage/2017033_210x300x50x2.jpg");
        mNames.add("Searching");
        mGenre.add("Drama,Triller");
        Description.add("After David Kim (John Cho)'s 16-year-old daughter goes missing, a local investigation is opened and a detective is assigned to the case. But 37 hours later and without a single lead, David decides to search the one place no one has looked yet, where all secrets are kept today: his daughter's laptop. In a hyper-modern thriller told via the technology devices we use every day to communicate, David must trace his daughter's digital footprints before she disappears forever." +
                "\n\nDirector: Aneesh Chaganty\n" +
                "Writers: Aneesh Chaganty, Sev Ohanian\n" +
                "Stars: John Cho, Debra Messing, Joseph Lee");
        year.add("2018");
        imdb.add("7,8");
        TrailerUrls.add("RtPtYDkc5eY");


        mImageUrls.add("https://my-hit.org/storage/1970274_210x300x50x2.jpg");
        mNames.add("Smallfoot");
        mGenre.add("Cartoon,Comedy,Fantasy");
        Description.add("A yeti named Migo is convinced that a human known only as \"Small Foot\" is real and has to prove to his tribe that it does exist with the help of Meechee and the S.E.S - Smallfoot Evidentiary Society. Written by Mark Mason Robledo" +
                "\n\nDirectors: Karey Kirkpatrick, Jason Reisig (co-director)\n" +
                "Writers: Karey Kirkpatrick (screenplay by), Clare Sera (screenplay by)\n" +
                "Stars: Channing Tatum, James Corden, Zendaya ");
        year.add("2018");
        imdb.add("7,2");
        TrailerUrls.add("1eHs4u1mRTo");





    }

    public void RecomendedFilmsInitImageBitmaps(){




        mImageUrls.add("https://my-hit.org/storage/291930_210x300x50x2.jpg");
        mNames.add("Shutter Island");
        mGenre.add("Triller,Mystery,Drama");
Description.add("It's 1954, and up-and-coming U.S. marshal Teddy Daniels is assigned to investigate the disappearance of a patient from Boston's Shutter Island Ashecliffe Hospital. He's been pushing for an assignment on the island for personal reasons, but before long he wonders whether he hasn't been brought there as part of a twisted plot by hospital doctors whose radical treatments range from unethical to illegal to downright sinister. Teddy's shrewd investigating skills soon provide a promising lead, but the hospital refuses him access to records he suspects would break the case wide open. As a hurricane cuts off communication with the mainland, more dangerous criminals \"escape\" in the confusion, and the puzzling, improbable clues multiply, Teddy begins to doubt everything - his memory, his partner, even his own sanity. Written by alfiehitchie"+
        "\n\nDirector: Martin Scorsese\n" +
        "Writers: Laeta Kalogridis (screenplay), Dennis Lehane (novel)\n" +
        "Stars: Leonardo DiCaprio, Emily Mortimer, Mark Ruffalo");
        year.add("2010");
        imdb.add("8,1");
        TrailerUrls.add("x_GWX8YuZ44");

        mImageUrls.add("https://my-hit.org/storage/230302_210x300x50x2.jpg");
        mNames.add("Unstoppable");
        mGenre.add("Action,Triller,Drama");
        Description.add(" runaway train carrying a cargo of toxic chemicals puts an engineer and his conductor in a race against time. They're chasing the runaway train in a separate locomotive and need to bring it under control before it derails on a curve and causes a toxic spill that will decimate a town. Written by ahmetkozan"+
                "\n\nDirector: Tony Scott\n" +
                "Writer: Mark Bomback\n" +
                "Stars: Denzel Washington, Chris Pine, Rosario Dawson");
        year.add("2010");
        imdb.add("6,8");
        TrailerUrls.add("fyuoIqeL-bc");

        mImageUrls.add("https://my-hit.org/storage/246891_210x300x50x2.jpg");
        mNames.add("I Am Legend");
        mGenre.add("Horror,Fantasy,Triller");
        Description.add("Robert Neville is a scientist who was unable to stop the spread of the terrible virus that was incurable and man-made. Immune, Neville is now the last human survivor in what is left of New York City and perhaps the world. For three years, Neville has faithfully sent out daily radio messages, desperate to find any other survivors who might be out there. But he is not alone. Mutant victims of the plague -- The Infected -- lurk in the shadows... watching Neville's every move... waiting for him to make a fatal mistake. Perhaps mankind's last, best hope, Neville is driven by only one remaining mission: to find a way to reverse the effects of the virus using his own immune blood. But he knows he is outnumbered... and quickly running out of time. Written by Warner Bros. Pictures"+
                "\n\nDirector: Francis Lawrence\n" +
                "Writers: Mark Protosevich (screenplay), Akiva Goldsman (screenplay) \n" +
                "Stars: Will Smith, Alice Braga, Charlie Tahan |");
        year.add("2007");
        imdb.add("7,2");
        TrailerUrls.add("FZSCXDpI2uQ");

        mImageUrls.add("https://my-hit.org/storage/293142_210x300x50x2.jpg");
        mNames.add("Source Code");
        mGenre.add("Fantasy,Action,Triller");
        Description.add("Army Captain Colter Stevens finds himself working on a special program where his consciousness can be inserted into another human being. The only catch is can only be there for 8 minutes at any given time. That morning, a bomb exploded on a commuter train just outside Chicago. He occupies the body of teacher going to work on that train and is confused as to what he is doing or why he is there as his last memory is of flying his helicopter on a combat mission in Afghanistan. Those in charge of the program explain to him that there is a bomb on the train, and that he must locate it. More importantly, he must identify the bomber as another bombing is expected later that day. He is also told however that he cannot change the past and can only gather information. As he develops a liking for his traveling companion Christina, he sets out to test that theory. Written by garykmcd"+
                "\n\nDirector: Duncan Jones\n" +
                "Writer: Ben Ripley\n" +
                "Stars: Jake Gyllenhaal, Michelle Monaghan, Vera Farmiga ");
        year.add("2011");
        imdb.add("7,5");
        TrailerUrls.add("Y-ECkOmt0OQ");

        mImageUrls.add("https://my-hit.org/storage/290237_210x300x50x2.jpg");
        mNames.add("John Q");
        mGenre.add("Triller,Drama");
        Description.add("John Quincy Archibald's son Michael collapses while playing baseball as a result of heart failure. John rushes Michael to a hospital emergency room where he is informed that Michael's only hope is a transplant. Unfortunately, John's insurance won't cover his son's transplant. Out of options, John Q. takes the emergency room staff and patients hostage until hospital doctors agree to do the transplant. Written by Anonymous"+
                "\n\nDirector: Nick Cassavetes\n" +
                "Writer: James Kearns\n" +
                "Stars: Denzel Washington, Robert Duvall, Gabriela Oltean");
        year.add("2002");
        imdb.add("7,1");
        TrailerUrls.add("GDABTczyP-0");



        mImageUrls.add("https://my-hit.org/storage/1362876_210x300x50x2.jpg");
        mNames.add("Eliza Graves");
        mGenre.add("Triller,Drama");
        Description.add("A couple of days before 1899 Christmas, the Oxford new graduate Dr. Edward Newgate arrives at the Stonehearst Asylum to complete training for his specialty of asylum medicine. He is met by armed men who take him to Dr. Silas Lamb, who welcomes his help and takes him under his wing. Edward is shocked to see the methods that Dr Lamb uses to run this asylum. He becomes infatuated with Eliza Graves, one of the patients who is a lady of status and does not seem to belong. One night, Edward overhears a knocking from the bowels of the facility and is shocked to find that everything is not as it seems in this place and that his uneasy feelings may be justified. What will Edward Choose? Written by Claudio Carvalho, Rio de Janeiro, Brazil"+
                "\n\nDirector: Brad Anderson\n" +
                "Writers: Edgar Allan Poe (based on a short story by), Joe Gangemi (screenplay by)\n" +
                "Stars: Kate Beckinsale, Jim Sturgess, David Thewlis ");
        year.add("2014");
        imdb.add("6,8");
        TrailerUrls.add("rawTSQ1RKQ0");

        mImageUrls.add("https://my-hit.org/storage/2017033_210x300x50x2.jpg");
        mNames.add("Searching");
        mGenre.add("Drama,Triller");
        Description.add("After David Kim (John Cho)'s 16-year-old daughter goes missing, a local investigation is opened and a detective is assigned to the case. But 37 hours later and without a single lead, David decides to search the one place no one has looked yet, where all secrets are kept today: his daughter's laptop. In a hyper-modern thriller told via the technology devices we use every day to communicate, David must trace his daughter's digital footprints before she disappears forever." +
                "\n\nDirector: Aneesh Chaganty\n" +
                "Writers: Aneesh Chaganty, Sev Ohanian\n" +
                "Stars: John Cho, Debra Messing, Joseph Lee");
        year.add("2018");
        imdb.add("7,8");
        TrailerUrls.add("RtPtYDkc5eY");

        mImageUrls.add("https://my-hit.org/storage/245227_210x300x50x2.jpg");
        mNames.add("Limitless");
        mGenre.add("Triller,Fantasy");
        Description.add("An action-thriller about a writer who takes an experimental drug that allows him to use 100 percent of his mind. As one man evolves into the perfect version of himself, forces more corrupt than he can imagine mark him for assassination. Out-of-work writer Eddie Morra's (Cooper) rejection by girlfriend Lindy (Abbie Cornish) confirms his belief that he has zero future. That all vanishes the day an old friend introduces Eddie to NZT, a designer pharmaceutical that makes him laser focused and more confident than any man alive. Now on an NZT-fueled odyssey, everything Eddie's read, heard or seen is instantly organized and available to him. As the former nobody rises to the top of the financial world, he draws the attention of business mogul Carl Van Loon (De Niro), who sees this enhanced version of Eddie as the tool to make billions. But brutal side effects jeopardize his meteoric ascent. With a dwindling stash and hit men who will eliminate him to get the NZT, Eddie must stay wired long ... Written by Relativity Media"+
                "\n\nDirector: Neil Burger\n" +
                "Writers: Leslie Dixon (screenplay), Alan Glynn (novel)\n" +
                "Stars: Bradley Cooper, Anna Friel, Abbie Cornish");
        year.add("2011");
        imdb.add("7,4");
        TrailerUrls.add("NcVB37h8pTA");

        mImageUrls.add("https://m.media-amazon.com/images/M/MV5BMTg1MTY2MjYzNV5BMl5BanBnXkFtZTgwMTc4NTMwNDI@._V1_UX182_CR0,0,182,268_AL_.jpg");
        mNames.add("Black Panther");
        mGenre.add("Fantasy,Action");
        Description.add("After the events of Captain America: Civil War, Prince T'Challa returns home to the reclusive, technologically advanced African nation of Wakanda to serve as his country's new king. However, T'Challa soon finds that he is challenged for the throne from factions within his own country. When two foes conspire to destroy Wakanda, the hero known as Black Panther must team up with C.I.A. agent Everett K. Ross and members of the Dora Milaje, Wakandan special forces, to prevent Wakanda from being dragged into a world war. Written by Editor"+
                "\n\nDirector: Ryan Coogler\n" +
                "Writers: Ryan Coogler, Joe Robert Cole | 2 more credits »\n" +
                "Stars: Chadwick Boseman, Michael B. Jordan, Lupita Nyong'o ");
        year.add("2018");
        imdb.add("7,4");
        TrailerUrls.add("x02xX2dv6bQ");

    }


    public void InitPosters(){

        mImageUrls.add("https://lumiere-a.akamaihd.net/v1/images/r_blackpanther_nowonhero_e6434d76.jpeg?region=0,0,2048,832");

        mImageUrls.add("https://www.caveshousehotelyallingup.com.au/wp-content/gallery/movies/murder-on-the-OE.jpg");

        mImageUrls.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQreP5X7zRggYuphIYjLALbQYQl6OjYZXKTcPwGY4CBHwcmJ9KKig");


        mImageUrls.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQS4P-N5AxFyOL-4liFFeABtwieqwymexkT95Tm1oBqUxlnBhIr");

        mImageUrls.add("https://thumbs.dfs.ivi.ru/storage39/contents/7/2/463b02a7363c29172750434f18ddad.jpg/1280x390/");
        mImageUrls.add("https://thumbs.dfs.ivi.ru/storage6/contents/f/e/9d33eb6474eaa0b5278ca689799311.jpg/1280x390/");
        mImageUrls.add("https://thumbs.dfs.ivi.ru/storage3/contents/b/6/fb8ded7a2d65e26b6fda96103e245f.jpg/1280x390/");
        mImageUrls.add("https://thumbs.dfs.ivi.ru/storage36/contents/2/e/d4b8b8f8b8b4eeff6606594f665d8d.jpg/1280x390/");
        mImageUrls.add("https://thumbs.dfs.ivi.ru/storage28/contents/1/7/b112e9fa353bf1f09f8be8005dceae.jpg/1280x390/");
        mImageUrls.add("https://thumbs.dfs.ivi.ru/storage37/contents/0/0/42e163374c2662dcdb8c3c0834c0a1.jpg/1280x390/");



    }

    public void InitInterestingPosts(){


        mNames.add("Popular serials");
        mNames.add("New cartoons");
        mNames.add("Best actions");
        mNames.add("Good movies for kids");

        mImageUrls.add("https://okdiario.com/img/series/2017/08/20/shameless-e1503254624771.jpg");
        mImageUrls.add("https://i.ytimg.com/vi/r80N4l8P2Ok/maxresdefault.jpg");
        mImageUrls.add("http://www.eurodead.net/wp-content/uploads/2018/08/2016-movies.jpg");
        mImageUrls.add("https://grkids.com/wp-content/uploads/2017/05/Flicks-free-summer-movies-celebration-1.jpg");

    }


    public void InitGenres(){


        mNames.add("Action");
        mNames.add("Fantasy");
        mNames.add("Triller");
        mNames.add("Drama");
        mNames.add("Cartoon");
        mNames.add("Horror");

        mImageUrls.add("https://img.icons8.com/ios/50/000000/crime.png");
        mImageUrls.add("https://img.icons8.com/ios/50/000000/fantasy.png");
        mImageUrls.add("https://img.icons8.com/ios/50/000000/horror.png");
        mImageUrls.add("https://img.icons8.com/ios/50/000000/drama.png");
        mImageUrls.add("https://img.icons8.com/ios/50/000000/finn.png");
        mImageUrls.add("https://img.icons8.com/ios/50/000000/mummy.png");

    }


}
