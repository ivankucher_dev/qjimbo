package com.iomis.qjimbo.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "UserDB";
    private static final String TABLE_NAME = "UserRegisterInfo";
    private static final String TABLE_NAME_WL = "WatchList";
    private static final String TABLE_NAME_FAVOURITES = "Favourites";
    private static final String KEY_ID = "id";
    private static final String COLUMN_FULLNAME = "fullname";
    private static final String COLUMN_WATCHLIST_ID = "watchlistid";
    private static final String COLUMN_FAVOURITES_ID = "favouritesid";
    private static final String COLUMN_USERNAME = "username";
    private static final String COLUMN_AVATAR = "avatar";
    private static final String COLUMN_EMAIL = "email";
    private static final String COLUMN_PASSWORD = "password";


    public  DBHelper (Context context){
        super(context, DATABASE_NAME, null , DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table "+TABLE_NAME+ "("
                + KEY_ID+" integer primary key autoincrement,"
                + COLUMN_FULLNAME+" text,"
                + COLUMN_USERNAME+ " text, "+ COLUMN_EMAIL+ " text, "+ COLUMN_PASSWORD + " INTEGER, " + COLUMN_AVATAR + " blob" + ");");


        db.execSQL("create table "+TABLE_NAME_WL+ "("
                + KEY_ID+" integer primary key autoincrement,"
                + COLUMN_WATCHLIST_ID + " text" + ");");

        db.execSQL("create table "+TABLE_NAME_FAVOURITES+ "("
                + KEY_ID+" integer primary key autoincrement,"
                + COLUMN_FAVOURITES_ID+" text" + ");");


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
;
    }
}
