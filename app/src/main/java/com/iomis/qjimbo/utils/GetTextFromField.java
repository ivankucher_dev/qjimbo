package com.iomis.qjimbo.utils;

import android.widget.EditText;

/*
This class gets Strings,which use as storage containers for Text from EditText field.


You should specify String in one of 2 constructors , where u want to get text, and after use one of 2 functions to convert text from

EditText field.


 */


public class GetTextFromField {

    String Firsttname;
    String Lastname;
    String Email;
    String Password;



    //Functions for Create Account Activity (Main Activity)

    public void getTextFromEditText(EditText firstname, EditText lastname, EditText email, EditText password) {
        this.Firsttname = firstname.getText().toString().trim();
        this.Lastname = lastname.getText().toString().trim();
        this.Email = email.getText().toString().trim();
        this.Password = password.getText().toString().trim();
    }

    public void getAllStrings(String InfoAboutUser[]) {
        InfoAboutUser[0] = this.Firsttname;
        InfoAboutUser[1] = this.Lastname;
        InfoAboutUser[2] = this.Email;
        InfoAboutUser[3] = this.Password;


    }


//Functions for SignIn activity

    public void getTextFromEditText(EditText email, EditText password) {
        this.Email = email.getText().toString().trim();
        this.Password = password.getText().toString().trim();
    }

    public void getAllStringSignIn(String InfoAboutUser[]) {
        InfoAboutUser[0] = this.Email;
        InfoAboutUser[1] = this.Password;
    }




}
