package com.iomis.qjimbo.utils;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class EndlessRecyclerView {



    public static void MakeEndlessRecyclerView(RecyclerView rv,final LinearLayoutManager linearLayoutManager,final int size){
        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int firstItemVisible = linearLayoutManager.findFirstVisibleItemPosition();
                int firstCompletelyItemVisible = linearLayoutManager.findFirstCompletelyVisibleItemPosition();


                if(firstItemVisible!=1  && firstItemVisible%size==1)
                    linearLayoutManager.scrollToPosition(1);

                if(firstCompletelyItemVisible==0)
                    linearLayoutManager.scrollToPositionWithOffset(size,0);

            }
        });
    }


}
