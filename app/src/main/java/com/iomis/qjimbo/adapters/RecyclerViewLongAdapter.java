package com.iomis.qjimbo.adapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.util.Log;
import android.util.Pair;
import android.view.*;
import android.widget.*;

import com.bumptech.glide.Glide;

import com.iomis.qjimbo.R;
import com.iomis.qjimbo.activity.FilmActivity;
import com.iomis.qjimbo.activity.InfoFilmActivity;
import com.iomis.qjimbo.activity.MainActivity;
import com.iomis.qjimbo.activity.MoviesAdapter;
import com.iomis.qjimbo.utils.DBHelper;
import com.makeramen.roundedimageview.RoundedImageView;
import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;



public class RecyclerViewLongAdapter extends RecyclerView.Adapter<RecyclerViewLongAdapter.ViewHolder>{

    private static final String TAG = "RecyclerViewAdapter";


    private ArrayList<String> mImages = new ArrayList<>();


    private Context mContext;


    private int resource;


    public RecyclerViewLongAdapter(Context context,ArrayList<String> images, int res ) {

        mImages = images;
        mContext = context;
        resource = res;

    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(resource, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called.");

        Glide.with(mContext)
                .load(mImages.get(position % mImages.size()))
                .asBitmap()
                .into(holder.image);





    }



    @Override
    public int getItemCount() {
        return mImages == null ? 0 : mImages.size() * 2 ;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView image;
        LinearLayout parentLayout;
        Activity activity;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            parentLayout = itemView.findViewById(R.id.linearLayout);
            activity = (Activity) mContext;
      image.setOnClickListener(this);

        }


        @Override
        public void onClick(View view) {

            int position = getAdapterPosition();
            switch (view.getId()) {
                case R.id.image :

                    Toast.makeText(mContext,"Its just s poster", Toast.LENGTH_SHORT).show();

                    break;



                default : break;



            }
        }
    }





}
