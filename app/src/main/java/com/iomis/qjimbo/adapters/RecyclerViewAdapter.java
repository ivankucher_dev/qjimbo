package com.iomis.qjimbo.adapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.util.Log;
import android.util.Pair;
import android.view.*;
import android.widget.*;

import com.bumptech.glide.Glide;

import com.iomis.qjimbo.R;
import com.iomis.qjimbo.activity.FilmActivity;
import com.iomis.qjimbo.activity.InfoFilmActivity;
import com.iomis.qjimbo.activity.MainActivity;
import com.iomis.qjimbo.activity.MoviesAdapter;
import com.iomis.qjimbo.utils.DBHelper;
import com.makeramen.roundedimageview.RoundedImageView;
import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;



public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>{

    private static final String TAG = "RecyclerViewAdapter";
    private static final String TABLE_WATCHLIST = "WatchList";
    private static final String COLUMN_WATCHLISTID = "watchlistid";
    private static final String TABLE_FAVOURITES = "Favourites";
    private static final String COLUMN_FAVOURITESID = "favouritesid";

    private ArrayList<String> mImageNames = new ArrayList<>();
    private ArrayList<String> mImages = new ArrayList<>();
    private ArrayList<String> mGenre = new ArrayList<>();
    private ArrayList<String> descr = new ArrayList<>();
    private ArrayList<String> year = new ArrayList<>();
    private ArrayList<String> imdb = new ArrayList<>();
    private ArrayList<String> trailer = new ArrayList<>();
    boolean ispopup;
    private Context mContext;
    ContentValues cv;
    SQLiteDatabase db;
    Cursor c;
    DBHelper dbHelper;

    private int resource;
    ActivityOptions optione;

    public RecyclerViewAdapter(Context context, ArrayList<String> imageNames, ArrayList<String> images,ArrayList<String> genre,ArrayList<String> description,ArrayList<String> mYear,ArrayList<String> mImdb,ArrayList<String> TrailerUrls, int res ) {
        mImageNames = imageNames;
        mImages = images;
        mGenre = genre;
        descr = description;
        mContext = context;
        resource = res;
        year = mYear;
        imdb = mImdb;
        trailer = TrailerUrls;
        ispopup=true;
    }

    public RecyclerViewAdapter(Context context, ArrayList<String> imageNames, ArrayList<String> images,ArrayList<String> genre,ArrayList<String> description,ArrayList<String> mYear,ArrayList<String> mImdb,ArrayList<String> TrailerUrls, int res,boolean isPopUp ) {
        mImageNames = imageNames;
        mImages = images;
        mGenre = genre;
        descr = description;
        mContext = context;
        resource = res;
        year = mYear;
        imdb = mImdb;
        trailer = TrailerUrls;
        ispopup= isPopUp;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(resource, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called.");

        Glide.with(mContext)
                .load(mImages.get(position))
                .asBitmap()
                .into(holder.image);

        holder.imageName.setText(mImageNames.get(position));
        holder.movieGenre.setText(mGenre.get(position));




    }




    @Override
    public int getItemCount() {
        return mImageNames.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView image;
        ImageView popupbutton;

        TextView imageName;
        TextView movieGenre;
        LinearLayout parentLayout;
        Activity activity;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            imageName = itemView.findViewById(R.id.moviename);
            movieGenre =  itemView.findViewById(R.id.genre);
            parentLayout = itemView.findViewById(R.id.linearLayout);
             activity = (Activity) mContext;
            image.setOnClickListener(this);
            if(ispopup) {
                popupbutton = itemView.findViewById(R.id.show_popup);
                popupbutton.setOnClickListener(this);

            }
        }


        @Override
        public void onClick(View view) {

            int position = getAdapterPosition();
            switch (view.getId()) {
                case R.id.image :
            Log.d(TAG, "onClick: clicked on: " + mImageNames.get(position));

            Toast.makeText(mContext, mImageNames.get(position), Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(mContext, InfoFilmActivity.class);
            intent.putExtra("image_url", mImages.get(position));
            intent.putExtra("image_name", mImageNames.get(position));
            intent.putExtra("image_genre", mGenre.get(position));
            intent.putExtra("image_descr", descr.get(position));
            intent.putExtra("image_year", year.get(position));
            intent.putExtra("image_imdb", imdb.get(position));
            intent.putExtra("trailer", trailer.get(position));
            Pair[] paire = new Pair[1];
            paire[0] = new Pair<View, String>(image, "TransitionFilmImage");
            optione = ActivityOptions.makeSceneTransitionAnimation(activity, paire);
            mContext.startActivity(intent,optione.toBundle());

            break;

                case R.id.show_popup:
                    showPopupMenu(view,position);
                    break;

                default : break;



            }
        }
    }


    private void showPopupMenu(View view, int poaition) {
      PopupMenu popup = new PopupMenu(mContext, view);
        MenuInflater inflater = popup.getMenuInflater();
        view.setBackgroundColor(R.color.popupmenu_dark_background);
        inflater.inflate(R.menu.menu_context, popup.getMenu());
        popup.setOnMenuItemClickListener(new RecyclerViewAdapter.MenuClickListener(poaition));
        popup.show();
    }


    class MenuClickListener implements PopupMenu.OnMenuItemClickListener {
        Integer pos;
        public MenuClickListener(int pos) {
            this.pos=pos;
        }


        private void addToDB(String TABLE_NAME, String COLUMN_NAME){

            cv = new ContentValues();
            dbHelper = new DBHelper(mContext);
            db = dbHelper.getWritableDatabase();
            //Check in database email is already in use
            String selectQuery = "SELECT  * FROM " + TABLE_NAME + " WHERE "
                    + COLUMN_NAME + " =\'" +mImageNames.get(pos) +"\'";
             c = db.rawQuery(selectQuery,null);


            if(c.getCount()==0) {
                cv.put(COLUMN_NAME, mImageNames.get(pos));
                //Insert in Database
                long rowID = db.insert(TABLE_NAME, null, cv);
                Log.d("myLogs", "row inserted, ID = " + rowID);
                db.close();
                Toast.makeText(mContext, mImageNames.get(pos)+" added successfully", Toast.LENGTH_SHORT).show();
            }
            else{
                Toast.makeText(mContext, mImageNames.get(pos)+" is already in the list", Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.action_favourite:
                    addToDB(TABLE_FAVOURITES,COLUMN_FAVOURITESID);
                    return true;
                case R.id.action_watch:
                   addToDB(TABLE_WATCHLIST,COLUMN_WATCHLISTID);


                    return true;
                case R.id.action_book:
                    Toast.makeText(mContext, "Booked Ticket for "+mImageNames.get(pos), Toast.LENGTH_SHORT).show();
                    return true;
                default:
            }
            return false;
        }
    }

}
