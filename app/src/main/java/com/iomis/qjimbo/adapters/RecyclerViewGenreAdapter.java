package com.iomis.qjimbo.adapters;



import android.app.Activity;
import android.app.ActivityOptions;
import android.app.VoiceInteractor;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.util.Log;
import android.util.Pair;
import android.view.*;
import android.widget.*;

import com.bumptech.glide.Glide;

import com.iomis.qjimbo.R;
import com.iomis.qjimbo.activity.*;
import com.iomis.qjimbo.utils.DBHelper;
import com.makeramen.roundedimageview.RoundedImageView;
import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;



public class RecyclerViewGenreAdapter extends RecyclerView.Adapter<RecyclerViewGenreAdapter.ViewHolder>{

    private static final String TAG = "RecyclerViewAdapter";


    private ArrayList<String> mImages = new ArrayList<>();
    private ArrayList<String> mNames = new ArrayList<>();
    Bundle bundle;


    private Context mContext;
    private ActivityOptions optione;


    private int resource;


    public RecyclerViewGenreAdapter(Context context,ArrayList<String> images,ArrayList<String> names, int res ) {

        mImages = images;
        mNames=names;
        mContext = context;
        resource = res;

    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(resource, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called.");

        Glide.with(mContext)
                .load(mImages.get(position))
                .asBitmap()
                .into(holder.image);
        holder.textPost.setText(mNames.get(position));


    }



    @Override
    public int getItemCount() {
        return mImages.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView image;
        TextView textPost;
        LinearLayout parentLayout;
        Activity activity;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.GenreImage);
            textPost = itemView.findViewById(R.id.textGenre);
            parentLayout = itemView.findViewById(R.id.linearlayout);
            activity = (Activity) mContext;
            image.setOnClickListener(this);

        }


        @Override
        public void onClick(View view) {

            int position = getAdapterPosition();
            switch (view.getId()) {
                case R.id.GenreImage :

                    Intent intent = new Intent(mContext, GenreActivity.class);
                    intent.putExtra("Genre", mNames.get(position));
                    bundle = ActivityOptionsCompat.makeCustomAnimation(mContext,
                            android.R.anim.fade_in, android.R.anim.fade_out).toBundle();
                    mContext.startActivity(intent,bundle);

                    break;




                default : break;



            }
        }
    }





}
